from uuid import uuid4
from hashlib import sha512
from random import sample

# from random import sample
from django.db import models

# Create your models here.
class Events(models.Model):
    event_name = models.CharField(verbose_name="Event Name", max_length=255)
    event_description = models.TextField(verbose_name="Event Description", max_length=255, blank=False, null=False)
    event_date = models.DateField()

    def __str__(self):
        return self.event_name


class EventRegistration(models.Model):
    event = models.ForeignKey(Events, on_delete=models.SET_NULL, blank=False, null=True)
    username = models.CharField(verbose_name="Name", max_length=255)
    mobile = models.CharField(verbose_name="Mobile", max_length=255)


    def __str__(self):
        return self.username


class EventFiles(models.Model):
    event = models.ForeignKey(Events, on_delete=models.SET_NULL, blank=False, null=True)
    event_file = models.FileField(upload_to='event_files/', null=True, verbose_name="Event Files")

    # def __str__(self):
    #     return self.event




