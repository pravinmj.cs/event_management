from django.contrib import admin
from app.models import Events, EventRegistration, EventFiles
from django.contrib.auth.models import Group


# Register your models here.
class EventsAdmin(admin.ModelAdmin):
    list_display = ["event_name", "event_description", "event_date"]

class EventRegistrationAdmin(admin.ModelAdmin):
    list_display = ["event", "username", "mobile"]

class EventFilesAdmin(admin.ModelAdmin):
    list_display = ["event", "event_file"]


admin.site.unregister(Group)
admin.site.register(Events, EventsAdmin)
admin.site.register(EventRegistration, EventRegistrationAdmin)
admin.site.register(EventFiles, EventFilesAdmin)


